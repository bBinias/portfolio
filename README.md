# Portfolio

This is a Data Science and AI related portfolio of Bartosz Binias.

Includes:
1. BCI (only CSP) -> add a lot of metrics and visualizations
2. FLight ML approach
3. Object recognition Keras Deep Learning
4. Regression in Python
5. Regression in R
6. A Hadoop example